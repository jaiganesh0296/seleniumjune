package org.testleaf.leaftaps.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {
	public CreateLeadPage() {}
	@FindBy(how=How.ID,using="createLeadForm_companyName")
	private WebElement comName;
	@FindBy(how=How.ID,using="createLeadForm_firstName")
	private WebElement firstName;
	@FindBy(how=How.ID,using="createLeadForm_lastName")
	private WebElement lastName;
	
	
	
	public CreateLeadPage enterDetails() {
		
		
		clearAndType(comName,"testleaf");
		clearAndType(firstName,"jai");
		clearAndType(lastName,"s");
		return this;
	}
		
	public VerifyLeadPage ClickCreateLead() {	
		click(locateElement("name", "submitButton"));
		return new VerifyLeadPage();
		
		
	}
}
