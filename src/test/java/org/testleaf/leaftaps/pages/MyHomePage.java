package org.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations {
	public MyHomePage() {}
	public MyLeadsPage clickMyLeads() {
		click(locateElement("link", "Leads"));
		return new MyLeadsPage();
	}
}
