package org.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations {
	public MyLeadsPage() {}
	public CreateLeadPage clickCreateLeads() {
		click(locateElement("link", "Create Lead"));
		return new CreateLeadPage();
	}
}
